var map = L.map('mapid').setView([3.4163102938387304, -76.5474677690421], 13);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/genovevas",
    success: function(result){
        console.log(result);
        result.genovevas.forEach(function(geno){
        L.marker(geno.ubicacion, {title: geno.id}).addTo(map);

        });
    }
});

