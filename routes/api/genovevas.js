var express = require('express');
var router = express.Router();
var genovevaController = require('../../controllers/api/genovevaControllerAPI');

router.get('/', genovevaController.genoveva_list);
router.post('/create', genovevaController.genoveva_create);
router.delete('/delete', genovevaController.genoveva_delete);

module.exports = router;