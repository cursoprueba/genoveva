var express = require('express');
var router = express.Router();
var genovevaController = require('../controllers/genoveva');

router.get('/', genovevaController.genoveva_list);
router.get('/create', genovevaController.genoveva_create_get);
router.post('/create', genovevaController.genoveva_create_post);
router.get('/:id/update', genovevaController.genoveva_update_get);
router.post('/:id/update', genovevaController.genoveva_update_post);
router.post('/:id/delete', genovevaController.genoveva_delete_post);

module.exports = router;