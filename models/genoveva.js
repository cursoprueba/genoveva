var Genoveva = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Genoveva.prototype.toString = function (){
    return 'id: ' + this.id + " | color: " + this.color;
}

Genoveva.allGenos = [];
Genoveva.add = function(aGeno){
    Genoveva.allGenos.push(aGeno);
}

Genoveva.findById = function(aGenoId){
    var aGeno = Genoveva.allGenos.find(x => x.id == aGenoId);
    if (aGeno)
        return aGeno;
    else
        throw new Error(`No existe una postre con el id ${aGenoId}`);
}

Genoveva.removeById = function(aGenoId){

    for(var i=0; i< Genoveva.allGenos.length; i++){
        if (Genoveva.allGenos[i].id == aGenoId){
            Genoveva.allGenos.splice(i, 1);
            break;
        }
    }
}

var a = new Genoveva(1, 'rojo', 'urbana', [-32.960912980991296,-60.653619123465035]);
var b = new Genoveva(2, 'blanca', 'urbana', [-32.963001482269036,-60.66464856483148]);

Genoveva.add(a);
Genoveva.add(b);

module.exports = Genoveva;