var Genoveva = require('../../models/genoveva');

exports.genoveva_list = function (req, res) { 
    res.status(200).json({
        genovevas: Genoveva.allGenos
    });
}

exports.genoveva_create = function (req, res) { 
    var geno= new Genoveva(req.body.id, req.body.color, req.body.modelo);
    geno.ubicacion = [req.body.lat, req.body.lng];
    Genoveva.add(geno);
    res.status(200).json({
        genoveva: geno
        });
    } 
    
    exports.genoveva_update= function (req, res) { 
        var geno = Genoveva.findById(req.params.id);
        geno.id = req.body.id;
        geno.color = req.body.color;
        geno.modelo = req.body.modelo;
        geno.ubicacion = [req.body.lat, req.body.lng];
        res.status(202).json({
            genoveva: geno
        });
    }
    
    exports.genoveva_delete = function (req, res) { 
        Genoveva.removeById(req.body.id);
        res.status(204).send();
    }
