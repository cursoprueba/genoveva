var Genoveva = require('../models/genoveva');

exports.genoveva_list = function(req, res){
    res.render('genovevas/index', {genos: Genoveva.allGenos});
}

exports.genoveva_create_get = function(req, res){
    res.render('genovevas/create');
}

exports.genoveva_create_post = function(req, res){
    var geno = new Genoveva(req.body.id, req.body.color, req.body.modelo);
    geno.ubicacion = [req.body.lat, req.body.lng];
    Genoveva.add(geno);

    res.redirect('/genovevas');
}

exports.genoveva_update_get = function(req, res){
    var geno = Genoveva.findById(req.params.id);

    res.render('genovevas/update', {geno});
}

exports.genoveva_update_post = function(req, res){
    var geno= Genoveva.findById(req.params.id);
    geno.id = req.body.id;
    geno.color = req.body.color;
    geno.modelo = req.body.modelo;
    geno.ubicacion = [req.body.lat, req.body.lng];

    res.redirect('/genovevas');
}

exports.genoveva_delete_post = function(req, res){
    Genoveva.removeById(req.body.id);

    res.redirect('/genovevas');
}